const btn = document.querySelector('.btn');
const list = document.querySelector('.list')

const urlIP = 'https://api.ipify.org/?format=json';
const apiIP = 'http://ip-api.com/json/';

btn.addEventListener('click', async () => {
    try {
        const {ip} = await getData(urlIP);
        const data = await getData(apiIP + ip + '?fields=1572889');

        Object.entries(data).forEach(([key, value]) => {
            const li = document.createElement('li');
            const keyName = key[0].toUpperCase() + key.slice(1);
            li.innerText = keyName + ': ' + (value || 'Unavailable');
            list.append(li);
        });
    } catch(e) {
        console.log(e);
    }
}, {once: true});

async function getData(url) {
    const response = await fetch(url);
    return await response.json();
}